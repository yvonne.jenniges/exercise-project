<!--
Copyright (c) 2021 Helmholtz-Zentrum Dresden - Rossendorf e.V.

SPDX-License-Identifier: MIT
-->

# GitLab CI - Getting Started Demo

This project serves as a getting started demo for GitLab CI.

## Build Instructions

### Requirements

* CMake > 3.14.0
* C++ Compiler

### How to build the example

From the project's main directory you can build and run the tests.

```bash
$ cmake -S . -B build
-- The C compiler identification is GNU 9.3.0
-- The CXX compiler identification is GNU 9.3.0
...
-- Build files have been written to: .../gitlab-ci-demo/build

$ cmake --build build
Scanning dependencies of target gtest
...
[100%] Built target gmock_main

$ cd build && ctest
Test project .../gitlab-ci-demo/build
...
100% tests passed, 0 tests failed out of 2

Total Test time (real) =   0.02 sec
```

### Run the example

```bash
$ export GITLAB_USER_NAME="Jane"
$ ./build/bin/helloWorld
Hello Jane!
```
